from rest_framework import permissions

 
class IsStaffOrAuthorizedUser(permissions.BasePermission):
    
    def has_object_permission(self, request, view, obj):
        """
        Allow only staff or user itself to perform operations on user/userprofile object
        """
        return  request.user.is_staff or obj == request.user

    
class IsStaffOrPostAuthor(permissions.BasePermission):

    def has_object_permission(self, request, view, obj):
        """
        Allow only staff or user itself to perform operations on post objects
        """
        return  request.user.is_staff or obj.created_by == request.user