from rest_framework import viewsets
from rest_framework.permissions import AllowAny ,IsAuthenticatedOrReadOnly

from users.api.permissions import IsStaffOrAuthorizedUser,IsStaffOrPostAuthor
from users.api.serializers import PostSerializer,UserSerializer
from users.models import Post,User


# Create your views here.
class UserView(viewsets.ModelViewSet):
    """
    User View
    """
    serializer_class = UserSerializer
    queryset = User.objects.all()
    
    def get_permissions(self):
        """
        Allow any to get and create user.
        Only staff or user itself to delete ,put or patch
        """
        if self.request.method == 'POST' or self.request.method == 'GET':
            return (AllowAny(),)
        else:
            return (IsStaffOrAuthorizedUser(),)

    
class PostView(viewsets.ModelViewSet):
    """
    Post view
    """
    serializer_class = PostSerializer
    queryset = Post.objects.all()
            
    def get_permissions(self):
        """
        Allow only Authenticate user to Post
        Allow only staff or author to edit its post
        """
        if self.request.method == 'POST' or self.request.method == 'GET':
            return (IsAuthenticatedOrReadOnly(),)
        else:
            return (IsStaffOrPostAuthor(),)
        
    def perform_create(self, serializer):
        """
        Save current user to the post 
        """
        serializer.save(created_by =self.request.user)
