from rest_framework import serializers

from users.models import Post, User, UserProfile

class UserProfileSerializer(serializers.ModelSerializer):
    """
    User profile serializer
    """
    class Meta:
        model = UserProfile
        fields = ('avatar','gender','birth_date','location')
        
class UserSerializer(serializers.ModelSerializer):
    """
    User serializer
    """
    profile = UserProfileSerializer(required=False)
    password = serializers.CharField(style={'input_type': 'password'}, write_only=True)
    class Meta:
        model = User
        fields = ('id','first_name', 'last_name','email','password','profile')
        read_only_field = 'id'
        
    def to_representation(self, obj):
        """
        Private information 'Email' in this case only to be shown to staff member or user itself,
        profile detials onyl to be shown to authneticated users
        """
        response = super(UserSerializer, self).to_representation(obj)
        if obj != self.context['request'].user and not self.context['request'].user.is_staff :
            response.pop('email')
        if not self.context['request'].user.is_authenticated:
            response.pop('profile'), response.pop('id')
        return response
    
    def create(self, validated_data):
        """
        Overriding create method to create User profile using single endpoint 
        """
        profile_data = validated_data.pop('profile')
        user = super(UserSerializer, self).create(validated_data)
        user.set_password(validated_data['password'])
        user.save()
        UserProfile.objects.create(user = user,**profile_data)
        return user
    
    def update(self, instance, validated_data):
        """
        Overriding update method to update User profile using single endpoint
        """
        profile_data = validated_data.pop('profile')
        """
        Update user data
        """
        super(UserSerializer, self).update(instance,validated_data)
        password = validated_data.get('password', None)
        if password:
            instance.set_password(password)
            instance.save()
        """
        Update user profile data
        """
        try:
            userprofile = instance.profile
        except UserProfile.DoesNotExist:
            userprofile = UserProfile.objects.create(user=instance, **profile_data)    
        UserProfileSerializer().update(userprofile,profile_data)
        
        return instance
    
class PostSerializer(serializers.ModelSerializer):
    """
    Post serializer
    """
    created_by = UserSerializer(read_only=True)
    class Meta:
        model = Post
        fields = ('id','title', 'body','pub_date','created_by')
        read_only_fields = ('pub_date','id')
        
    def to_representation(self, obj):
        """
        Hidding post body form anonymous users
        """
        response = super(PostSerializer, self).to_representation(obj)
        if not self.context['request'].user.is_authenticated:
            response.pop('body') , response.pop('id')
        return response