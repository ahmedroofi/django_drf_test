# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib.auth import authenticate, login , logout
from django.views.generic import View
from django.http import JsonResponse
from django.shortcuts import render


class UserloginView(View):
    def get(self,request):
        email = request.GET.get('email',None)
        password = request.GET.get('password' , None)
        
        if email and password:
            user = authenticate(email = email , password = password)
            if user is not None :                
                if user.is_active:
                        login(request,user)
                        return JsonResponse({"message": "logged in"})
                else:
                    return JsonResponse({"message": "User not active"},status=401)
            else:
                return JsonResponse({"message": "Incorrect Username/Password"},status=401)
        else:
             return JsonResponse({"message": "No Username/Password provided"},status=403)


class UserlogoutView(View):
    def get(self, request):
        logout(request)     
        return JsonResponse({"message": "Logout"})
            
            

    
    