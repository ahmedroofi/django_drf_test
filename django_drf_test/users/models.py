# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from datetime import datetime

from django.contrib.auth.models import AbstractUser
from django.db import models
from django.utils.translation import ugettext_lazy as _

from users.managers import UserManager
# Create your models here.


class User(AbstractUser):
    """
    Custom User Models
    """
    username = None
    email = models.EmailField(_('email address'), unique=True)
    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = []
    objects = UserManager()


class UserProfile(models.Model):
    """
    User Profile Model
    """
    GENDER_CHOICES = (('m', _('Male')), ('f', _('Female')))
    user = models.OneToOneField(User, on_delete=models.CASCADE,related_name="profile")
    gender = models.CharField(max_length=1, choices=GENDER_CHOICES,blank=True, null=True)
    location = models.CharField(max_length=30, blank=True)
    birth_date = models.DateField(null=True, blank=True)
    avatar = models.ImageField(upload_to='avatars/', null=True, blank=True)
    
    def __str__(self):
        return self.user.email

    
class Post(models.Model):
    """
    User Posts Model
    """
    title = models.CharField(max_length=200)
    pub_date = models.DateTimeField(default = datetime.now())
    body = models.TextField()
    created_by = models.ForeignKey(User)
    
    def __str__(self):
        return "%s (%s)" % (self.title, self.created_by)
