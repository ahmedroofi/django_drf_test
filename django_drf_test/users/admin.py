# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from django.contrib.auth.admin import UserAdmin as DjangoUserAdmin
from django.utils.translation import ugettext_lazy as _

from users.models import User, UserProfile, Post
# Register your models here.


class ProfileInline(admin.StackedInline):
    """
    Inline UserProfile model for User 
    """
    model = UserProfile
    can_delete = False
    verbose_name_plural = 'Profile'
    fk_name = 'user'


@admin.register(User)
class UserAdmin(DjangoUserAdmin):
    """
    Register/Display User Model to Django Admin
    """
    inlines = (ProfileInline, )
    fieldsets = (
        (None, {'fields': ('email', 'password')}),
        (_('Personal info'), {'fields': ('first_name', 'last_name')}),
        (_('Permissions'), {'fields': ('is_active', 'is_staff', 'is_superuser',
                                       'groups', 'user_permissions')}),
        (_('Important dates'), {'fields': ('last_login', 'date_joined')}),
    )
    list_display = ('email', 'first_name', 'last_name', 'is_staff')
    search_fields = ('email', 'first_name', 'last_name')
    ordering = ('email',)


@admin.register(Post)
class PostAdmin(admin.ModelAdmin):
    """
    Register Post model in Django Admin
    """
    list_display = ('title', 'pub_date', 'created_by')
    search_fields = ('title',)
    ordering = ('pub_date',)
