from rest_framework import routers

from django.conf import settings
from django.conf.urls import url, include
from django.conf.urls.static import static

from users import views as vviews
from users.api import views

 
router = routers.DefaultRouter()
router.register(r'users', views.UserView)
router.register(r'posts', views.PostView)

urlpatterns =[
    url(r'^api/', include(router.urls)),
    url(r'^login/$', vviews.UserloginView.as_view(), name='login'),
    url(r'^logout/$', vviews.UserlogoutView.as_view(), name='logout'),
]+ static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)