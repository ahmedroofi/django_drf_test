# Django DRF Test

Solution for Django DRF Test

## Setup 
```
Open command prompt
```
```
Move to django_drf_test directory
```
```
Run command  ‘pip install –r requirement.txt’
```

## Run Migrations
```
Move to django_drf_test  project directory
```
```
Run command ‘python manage.py migrate’
```

## Create Super user or admin user for django admin
```
Run command ‘python manage.py createsuperuser’
```
```
Enter email address
```
```
Enter password
```

## Run Project
```
Python manage.py runserver
```

## Django admin URL:

Use super user or admin credentials created to login the django admin.
```
http://localhost:8000/admin/
```

## Test Instructions

The APIs are based on session base authentication.
```	
User API endpoint:
http://localhost:8000/api/users/
Create user using post request.
For testing purpose you can login and logout with the user created by urls:
http://localhost:8000/login/?email=user@new.com&password=admin
http://localhost:8000/logout/	
Post API endpoint:
http://localhost:8000/api/posts/
```
